package peko.honey.readToken;

import android.content.Context;
import android.content.SharedPreferences;

import peko.honey.api.ApiConstants;


/**
 * Created by Giang on 5/5/2016.
 */
public class WriteReadToken implements ApiConstants {

    public static String Token, username, email, phone, avatar, referralCode, fullName, success,currentNameSchool;
    public static int id, totalPoints, staticPoin;
    public static boolean verified;

    public void getPref(String key, Context context) {
        SharedPreferences userDetails = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        Token = userDetails.getString("Token", "");
        username = userDetails.getString("username", "");
        email = userDetails.getString("email", "");
        fullName = userDetails.getString("fullName", "");
        phone = userDetails.getString("phone", "");
        avatar = userDetails.getString("avatar", "");
        success = userDetails.getString("success", null);
        referralCode = userDetails.getString("referralCode", "");
        currentNameSchool = userDetails.getString("schoolName", "");
        id = userDetails.getInt("id", 0);
        staticPoin = userDetails.getInt("staticPoin", 0);

        verified = userDetails.getBoolean("isVerifi", false);
        totalPoints = userDetails.getInt("totalPoints", 0);

        System.out.println("Access token : " + Token);
        System.out.println("id : " + id);
        System.out.println("static poin : " + staticPoin);
    }


}
