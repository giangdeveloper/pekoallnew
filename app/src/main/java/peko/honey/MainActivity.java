package peko.honey;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;

import peko.honey.adapter.RestaurentAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.common.MyActivity;
import peko.honey.entity.RestaurentEntity;
import peko.honey.fragment.DetailShopFragment;
import peko.honey.fragment.ExchangeGiftFragment;
import peko.honey.onclick.OnclickRestaurent;

import java.util.ArrayList;

public class MainActivity extends MyActivity implements OnclickRestaurent,ApiConstants{

    RestaurentEntity entity;
    RestaurentAdapter adapter;
    ArrayList<RestaurentEntity> result = new ArrayList<>();
    RecyclerView rcResList;
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_restaurent);




    }



    private BroadcastReceiver onNotice = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //   checkmove = false;
            String value = intent.getExtras().getString(VALUE);

            if (value.equals(DOIQUA)) {

                //  buildPayMeg(getString(R.string.app_name), "Scan QR Code");

                ExchangeGiftFragment exchangeGiftFragment = new ExchangeGiftFragment();
                addFragment(exchangeGiftFragment, ExchangeGiftFragment.TAG,R.id.frameHome,true);


            }


        }
    };


    @Override
    public void onResume() {

        IntentFilter iff = new IntentFilter(BROADMAIN);
        registerReceiver(onNotice, iff);

        super.onResume();
    }

    @Override
    public void onPause() {

        unregisterReceiver(onNotice);
        super.onPause();
    }




    @Override
    public void onclickRestaurent(View v, RestaurentEntity entity) {
        DetailShopFragment detailShopFragment = new DetailShopFragment();
        addFragment(detailShopFragment,DetailShopFragment.TAG,R.id.frameHome,true);
    }

    @Override
    public void onclickExChange(View v, RestaurentEntity entity) {

    }

//    @Override
//    public void onclickDoiQua(View v, RestaurentEntity entity) {
//
//    }

    @Override
    public void onclickViewLocation(View v, RestaurentEntity entity) {

    }


    public void refreshActionBar(View contentView) {
        TextView tvBack = (TextView) contentView.findViewById(R.id.tvBack);

        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    activity.onBackPressed();
                }
            });

        }
    }



}
