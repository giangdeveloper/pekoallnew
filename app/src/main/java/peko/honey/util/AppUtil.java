/**
 *
 */
package peko.honey.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.NotificationManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;




// TODO: Auto-generated Javadoc

/**
 * The Class AppUtil.
 *
 * @author SyND
 */
public class AppUtil implements AppConstants {

    /**
     * The Constant separator.
     */
    public static final String separator = System.getProperty("line.separator");

    public static enum AnimatorType {
        VERTICAL, HORIZONTAL
    }

    public static void log(String log) {
        Log.e("abc", "" + log);
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void showToast(Context context, int textId) {
        Toast.makeText(context, textId, Toast.LENGTH_LONG).show();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidUrl(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.WEB_URL.matcher(target).matches();
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> appList = pm.getInstalledApplications(0);
        boolean lineInstallFlag = false;
        for (ApplicationInfo ai : appList) {
            if (ai.packageName.equals(packageName)) {
                lineInstallFlag = true;
                break;
            }
        }
        return lineInstallFlag;
    }

    public static ArrayList<String> getGoogleAccounts(Context context) {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");
        ArrayList<String> emailList = new ArrayList<>();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                emailList.add(account.name);
            }
        }
        return emailList;
    }

    public static void shareViaApp(Context context, String packageName, String className, String text, File file) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setClassName(packageName, className);
        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        if (file != null && file.exists()) {
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        } else {
            shareIntent.setType("text/plain");
        }
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(shareIntent);
    }

    public static boolean isUrl(String filePath) {
        return filePath != null && filePath.startsWith("http");
    }

    public static File getAppSaveFolder(String folderName) {
        String folder = Environment.getExternalStorageDirectory() +
                "/" + TAG +
                "/" + folderName;
        File dir = new File(folder);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static String downloadImage(String imageUrl) {
        String fileName = "Saved_" + DateTimeHelper.currentCalendar(
                DateTimeHelper.FORMAT_FILE) + "_" + getFullName(imageUrl);
        File file = new File(getAppSaveFolder(FOLDER_SAVE_IMAGE), fileName);
        FileOutputStream fos = null;
        InputStream input = null;
        try {
            input = new URL(imageUrl).openStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            fos = null;
            bitmap.recycle();
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
                input = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                fos.close();
                fos = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getHashKey(Context context) {
        String hashKey = null;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    PACKAGE,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                hashKey = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                AppUtil.log("===============getHashKey: " + hashKey);
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hashKey;
    }

    public static String getUniqueDeviceKey(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    @SuppressWarnings("deprecation")
    public static boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static void vibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(200);
    }

    public static void setClipboard(Context context, String text) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(TAG, text);
        clipboardManager.setPrimaryClip(clipData);
    }

    private static ValueAnimator slideAnimator(int start, int end, final View view, final AnimatorType type) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (type == AnimatorType.VERTICAL) {
                    layoutParams.height = value;
                } else {
                    layoutParams.width = value;
                }
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    public static void expandCollapseView(final View view, final int fromSize, final int toSize, final AnimatorType type, final Animator.AnimatorListener listener) {
        view.setVisibility(View.VISIBLE);
        ValueAnimator mAnimator;
        mAnimator = slideAnimator(fromSize, toSize, view, type);
        if (listener != null) {
            mAnimator.addListener(listener);
        }
        mAnimator.start();
    }

    public static void cancelNotification(Context context) {
        NotificationManager notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
    }

    public static int getIndex(String[] array, String value) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (value.equals(array[i])) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static void requestRefresh(Context context, Bundle bundle, Class<?>... classes) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < classes.length; i++) {
            builder.append(classes[i].getName() + ",");
        }
        Intent refreshIntent = new Intent(ACTION_REFRESH);
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString(KEY_CLASS_NAME, builder.toString());
        refreshIntent.putExtras(bundle);
        context.sendBroadcast(refreshIntent);
    }

    public static String getRegistrationId(Context context) {
        String registrationId = getPref(context, PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = getPref(context, PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getVersionCode(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    public static String getFileDir() {
        return Environment.getExternalStorageDirectory() + "/" + TAG;
    }

    public static String getCurrentTimeFileName(String ext) {
        return System.currentTimeMillis() + ext;
    }

    public static File createCurrentTimeFile(String ext) {
        return createFile(getFileDir(), getCurrentTimeFileName(ext));
    }

    public static File createFile(String dirPath, String fileName) {
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    public static void deleteFile(final String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    File file = new File(filePath);
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public static void deleteFile(final Context context, final Uri uri) {
        if (uri == null) {
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    File file = new File(getRealPath(context, uri));
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public static String downloadImage(Context context, String imageUrl, String filePath) {
        InputStream input = null;
        try {
            input = new URL(imageUrl).openStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return saveBitmap(context, bitmap, filePath);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
                input = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String saveBitmap(Context context, Bitmap bitmap, String filePath) {
        String fileName = getFullName(filePath).trim();
        String dirPath = filePath.replace(fileName, "").trim();
        File file = createFile(dirPath, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            fos = null;
            bitmap.recycle();
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
                fos = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static final String getDeviceId(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imeiNo = tm.getDeviceId();
        return imeiNo;
    }

    public static double kmToMiles(double km) {
        return km * 0.621371f;
    }

    public static double milesToKm(double miles) {
        return miles * 1.60934f;
    }

    public static String formatNumber(String text) {
        try {
            double number = Double.parseDouble(text);
            return formatNumber(number);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String formatNumber(long number) {
        return NumberFormat.getInstance().format(number);
    }

    public static String formatNumber(double number) {
        return NumberFormat.getInstance().format(number);
    }

    public static Picasso getPicasso(Context context) {
        return Picasso.with(context);
    }

    public static void invalidatePicasso(final Context context, final ArrayList<String> list) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                for (String item : list) {
                    try {
                        AppUtil.getPicasso(context).invalidate(item);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }
        }.execute();
    }

    public static void invalidatePicasso(final Context context, final String item) {
        if(item == null){
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    AppUtil.getPicasso(context).invalidate(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public static RequestCreator getPicassoUrl(Context context, String url) {
        if (TextUtils.isEmpty(url)) {
            return getPicasso(context).load("http://");
        }
        return getPicasso(context).load(url);
    }

    public static RequestCreator getPicassoAsset(Context context, String filePath) {
        return getPicasso(context).load(getAssetUri(filePath));
    }

    public static RequestCreator getPicassoUri(Context context, Uri uri) {
        return getPicasso(context).load(uri);
    }

    public static RequestCreator getPicassoFile(Context context, String file) {
        return getPicasso(context).load(new File(file));
    }

    public static RequestCreator getPicassoResource(Context context, int resId) {
        return getPicasso(context).load(resId);
    }

    public static RequestCreator getPicassoFileOrUrl(Context context, String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            filePath = "image";
        }
        if (isUrl(filePath)) {
            return getPicassoUrl(context, filePath);
        } else {
            return getPicassoFile(context, filePath);
        }
    }

    public static void toast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void toast(Context context, int textId) {
        Toast.makeText(context, textId, Toast.LENGTH_SHORT).show();
    }

    public static String encodeBase64(String text) {
        try {
            byte[] data = text.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        s = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        //System.out.println(s);
        return s;
    }
    //
    //	public static boolean isValidPhoneNumber(String phone, String regionCode){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		PhoneNumber phoneNumber;
    //		try {
    //			phoneNumber = phoneUtil.parse(phone, regionCode);
    //			return phoneUtil.isValidNumber(phoneNumber);
    //		} catch (NumberParseException e) {
    //			e.printStackTrace();
    //		}
    //		return false;
    //	}
    //
    //	/**
    //	 * +84xxx
    //	 */
    //	public static String formatPhoneNumber(String phone, String regionCode){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		try {
    //			PhoneNumber phoneNumber = phoneUtil.parse(phone, regionCode);
    //			return phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
    //		} catch (NumberParseException e) {
    //			e.printStackTrace();
    //		}
    //		return phone;
    //	}
    //
    //	/**
    //	 * +84xxx => xxx
    //	 */
    //	public static String getPhoneNumber(String phone){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		try {
    //			PhoneNumber phoneNumber = phoneUtil.parse(phone, "");
    //			return phoneUtil.format(phoneNumber, PhoneNumberFormat.NATIONAL).replaceAll("\\s+", "").trim();
    //		} catch (NumberParseException e) {
    //			e.printStackTrace();
    //		}
    //		return phone;
    //	}
    //
    //	public static ArrayList<CountryEntity> getCountryList(){
    //		ArrayList<CountryEntity> list = new ArrayList<CountryEntity>();
    //		ArrayList<String> countries = new ArrayList<String>();
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		for (Locale locale : Locale.getAvailableLocales()) {
    //			String name = locale.getDisplayCountry();
    //			if (name.trim().length() > 0 && !countries.contains(name)) {
    //				countries.add(name);
    //				String regionCode = locale.getCountry();
    //				list.add(new CountryEntity(name, regionCode, phoneUtil.getCountryCodeForRegion(regionCode)));
    //			}
    //		}
    //		Collections.sort(list, new Comparator<CountryEntity>() {
    //			@Override
    //			public int compare(CountryEntity lhs, CountryEntity rhs) {
    //				return lhs.name.compareTo(rhs.name);
    //			}
    //		});
    //		countries = null;
    //		return list;
    //	}
    //
    //	/**
    //	 * 84
    //	 */
    //	public static int getCountryCode(String phone){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		try {
    //			PhoneNumber phoneNumber = phoneUtil.parse(phone, "");
    //			return phoneUtil.getCountryCodeForRegion(phoneUtil.getRegionCodeForNumber(phoneNumber));
    //		} catch (NumberParseException e) {
    //			e.printStackTrace();
    //		}
    //		return DEFAULT_COUNTRY_CODE;
    //	}
    //
    //	/**
    //	 * 84
    //	 */
    //	public static int getMyCountryCode(Context context){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		return phoneUtil.getCountryCodeForRegion(getMyRegionCode(context));
    //	}
    //
    //	/**
    //	 * VN
    //	 */
    //	public static String getMyRegionCode(Context context){
    //		String country = context.getResources().getConfiguration().locale.getCountry();
    //		return country;
    //	}
    //
    //	public static CountryEntity getMyCountryEntity(Context context){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		Locale locale = context.getResources().getConfiguration().locale;
    //		String name = locale.getDisplayCountry();
    //		String regionCode = locale.getCountry();
    //		return new CountryEntity(name, regionCode, phoneUtil.getCountryCodeForRegion(regionCode));
    //	}
    //
    //	public static String convertPhoneNumber(String phoneNumber, CountryEntity countryEntity){
    //		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    //		try {
    //			PhoneNumber swissMobileNumber = phoneUtil.parse(phoneNumber, countryEntity.regionCode);
    //			return phoneUtil.format(swissMobileNumber, PhoneNumberFormat.E164);
    //		} catch (NumberParseException e) {
    //			e.printStackTrace();
    //		}
    //		return countryEntity.countryCode + phoneNumber;
    //	}

    public static String decodeBase64(String text) {
        try {
            byte[] data = Base64.decode(text, Base64.DEFAULT);
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static String toString(ArrayList<Integer> list) {
        StringBuilder builder = new StringBuilder();
        for (Integer obj : list) {
            builder.append(obj.toString() + ",");
        }
        if (builder.length() > 0) {
            builder = builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    public static int[] getIntArray(Context context, int arrayId) {
        TypedArray array = context.getResources().obtainTypedArray(arrayId);
        int length = array.length();
        int[] ids = new int[length];
        for (int i = 0; i < length; i++) {
            ids[i] = array.getResourceId(i, 0);
        }
        array.recycle();
        return ids;
    }

    public static SpannableString createSpannableString(String text, String key, ClickableSpan clickableSpan) {
        SpannableString spannable = new SpannableString(text);
        int start = text.indexOf(key);
        spannable.setSpan(clickableSpan, start, start + key.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public static String[] getStringArray(Context context, int arrayId) {
        return context.getResources().getStringArray(arrayId);
    }

    /**
     * Gets the version code.
     *
     * @param context the context
     * @return the version code
     */
    public static int getVersionCode(Context context) {
        int v = 1;
        try {
            v = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
        }
        return v;
    }

    /**
     * Px to dp.
     *
     * @param context the context
     * @param px      the px
     * @return the float
     */
    public static float pxToDp(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    /**
     * Dp to px.
     *
     * @param context the context
     * @param dp      the dp
     * @return the float
     */
    public static float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static int screenWidth(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.widthPixels;
    }

    public static int screenHeight(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.heightPixels;
    }

    /**
     * Gets the version name.
     *
     * @param context the context
     * @return the version name
     */
    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null) {
            return cm.getActiveNetworkInfo().isConnected();
        }
        return false;
    }

    public static boolean isGpsAvailable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the app name.
     *
     * @param context the context
     * @return the app name
     */
    public static String getAppName(Context context) {
        return context.getString(R.string.app_name);
    }

    /**
     * Sets the pref.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void setPref(Context context, String key, int value) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        pref.edit().putInt(key, value).commit();
    }

    /**
     * Gets the pref.
     *
     * @param context      the context
     * @param key          the key
     * @param defaultValue the default value
     * @return the pref
     */
    public static int getPref(Context context, String key, int defaultValue) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return pref.getInt(key, defaultValue);
    }

    /**
     * Sets the pref.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void setPref(Context context, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        pref.edit().putString(key, value).commit();
    }

    /**
     * Gets the pref.
     *
     * @param context      the context
     * @param key          the key
     * @param defaultValue the default value
     * @return the pref
     */
    public static String getPref(Context context, String key, String defaultValue) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return pref.getString(key, defaultValue);
    }

    public static void setPref(Context context, String key, boolean value) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        pref.edit().putBoolean(key, value).commit();
    }

    public static boolean getPref(Context context, String key, boolean defaultValue) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return pref.getBoolean(key, defaultValue);
    }

    public static boolean clearPref(Context context) {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return pref.edit().clear().commit();
    }

    /**
     * Hide keyboard.
     */
    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }

    public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        view.requestFocus();
    }

    /**
     * Dp to px.
     *
     * @param context the context
     * @param dp      the dp
     * @return the int
     */
    public static int dpToPx(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }

    /**
     * Px to dp.
     *
     * @param context the context
     * @param px      the px
     * @return the int
     */
    public static int pxToDp(Context context, int px) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px,
                context.getResources().getDisplayMetrics());
    }

    public static Uri getAssetUri(String filePath) {
        return Uri.parse("file:///android_asset/" + filePath);
    }

    public static Uri createImageUri(Context context) {
        ContentValues values = new ContentValues();
        values.put(Images.Media.TITLE, getCurrentTimeFileName(".jpg"));
        return context.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    public static File createImageFile() {
        return createFile(getFileDir(), System.currentTimeMillis() + ".jpg");
    }

    public static File createImageFile(String extra) {
        return createFile(getFileDir(), System.currentTimeMillis() + extra + ".jpg");
    }

    public static Uri captureImage(Activity activity, int requestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri mImageCaptureUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg"));

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                mImageCaptureUri);
        try {
            intent.putExtra("return-data", true);
            activity.startActivityForResult(intent, requestCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mImageCaptureUri;
    }

    public static Uri captureImage(Fragment fragment, int requestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri mImageCaptureUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg"));

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                mImageCaptureUri);
        try {
            intent.putExtra("return-data", true);
            fragment.startActivityForResult(intent, requestCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mImageCaptureUri;
    }

    public static void pickImage(Activity activity, int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent,
                "Complete action using"), requestCode);
    }

    public static void pickImage(Fragment fragment, int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        fragment.startActivityForResult(Intent.createChooser(intent,
                "Complete action using"), requestCode);
    }

    public static File cropImage(Activity activity, Uri uri, int requestCode) {
        File tempFile = new File(Environment
                .getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("outputX", 800);
        intent.putExtra("outputY", 800);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
        activity.startActivityForResult(intent, requestCode);
        return tempFile;
    }

    public static File cropImage(Fragment fragment, Uri uri, int requestCode) {
        File tempFile = new File(Environment
                .getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("outputX", 800);
        intent.putExtra("outputY", 800);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
        fragment.startActivityForResult(intent, requestCode);
        return tempFile;
    }

    public static void goToMail(Activity activity, String email) {
        try {
            Intent intent = activity.getPackageManager().getLaunchIntentForPackage("com.google.android.gm");
            intent.setData(Uri.parse(email));
            activity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Intent getIntentCall(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        return intent;
    }

    public static Intent getIntentSettingGPS() {
        return new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    }

    public static Intent getIntentSettingConnection() {
        return new Intent(Settings.ACTION_WIFI_SETTINGS);
    }

    public static Intent getIntentShare(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        return intent;
    }

    public static void showOnMap(Activity activity, double lat, double lng, String info) {
        Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lng
                + "?q=" + lat + "," + lng + "(" + info + ")");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(mapIntent);
        }
    }

    public static Intent getIntentViewLink(String url) {
        if (!url.startsWith("http://")) {
            url = "http://" + url;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        return intent;
    }

    public static Intent getIntentShareEmail(Context context, String subject, String text, File file) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("message/rfc822");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        if (file != null && file.exists()) {
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        }
        return Intent.createChooser(shareIntent, null);
    }

    public static void removeUnderlines(TextView textView) {
        try {
            removeUnderlines((Spannable) textView.getText());
        } catch (Exception e) {
        }
    }

    public static void removeUnderlines(EditText editText) {
        try {
            removeUnderlines((Spannable) editText.getText());
        } catch (Exception e) {
        }
    }

    public static void removeUnderlines(Spannable spanText) {
        URLSpan[] spans = spanText.getSpans(0, spanText.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = spanText.getSpanStart(span);
            int end = spanText.getSpanEnd(span);
            spanText.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            spanText.setSpan(span, start, end, 0);
        }
    }

    private static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }

        public void updateDrawState(TextPaint textPaint) {
            super.updateDrawState(textPaint);
            textPaint.setUnderlineText(false);
        }
    }

    public static Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String title = TAG + "_" + System.currentTimeMillis();
        String path = Images.Media.insertImage(context.getContentResolver(), inImage, title, null);
        return Uri.parse(path);
    }

    public static String getRealPath(Context context, Uri uri) {
        if (new File(uri.getPath()).exists()) {
            return uri.getPath();
        }
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk >= 19) {
            String filePath = "";
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {Images.Media.DATA};

            // where id is equal to
            String sel = Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } else if (sdk >= 11 && sdk <= 18) {
            String[] proj = {Images.Media.DATA};
            String result = null;

            CursorLoader cursorLoader = new CursorLoader(
                    context,
                    uri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();

            if (cursor != null) {
                int column_index =
                        cursor.getColumnIndexOrThrow(Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        } else {
            String[] proj = {Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
    }

    public static String getRealPath(Context context, Bitmap inImage) {
        return getRealPath(context, getImageUri(context, inImage));
    }

    public static String[] insertArray(String[] array, int position, String value) {
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(array));
        list.add(position, value);
        return list.toArray(new String[list.size()]);
    }

    public static int indexOf(String[] array, String value) {
        return Arrays.asList(array).indexOf(value);
    }

    public static void releaseByteArray(byte[] bytes) {
        bytes = null;
        System.gc();
    }

    public static void releaseBitmap(Bitmap bm) {
        if (bm != null && !bm.isRecycled()) {
            bm.recycle();
        }
        bm = null;
        System.gc();
    }

    public static String getFullName(String path) {
        int index = path.lastIndexOf("/");
        if (index < path.length()) {
            return path.substring(index + 1, path.length());
        }
        return path;
    }

    public static String getShortName(String path) {
        String fullName = getFullName(path);
        int index = fullName.lastIndexOf(".");
        if (index > 0) {
            return fullName.substring(0, index);
        }
        return fullName;
    }

    public static String downloadAndSavePhoto(Context context, String url) {
        String folder = Environment.getExternalStorageDirectory() + "/" + TAG;
        File dir = new File(folder);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, getFullName(url));
        InputStream input = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoInput(true);
            connection.connect();
            input = connection.getInputStream();
            Bitmap bm = BitmapFactory.decodeStream(input);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bm.compress(url.endsWith("png") ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, 100, bytes);

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bytes.toByteArray());
            fos.close();
            if (file.exists() && file.canRead()) {
                return file.getAbsolutePath();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
