package peko.honey.entity;

/**
 * Created by macshop1 on 11/17/17.
 */

public class ExChangeGiftEntity {
    int id;
    String cover;
    String name;
    int poin;

    public ExChangeGiftEntity() {
    }

    public ExChangeGiftEntity(int id, String cover, String name, int poin) {
        this.id = id;
        this.cover = cover;
        this.name = name;
        this.poin = poin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoin() {
        return poin;
    }

    public void setPoin(int poin) {
        this.poin = poin;
    }
}
