package peko.honey.entity;

/**
 * Created by macshop1 on 11/17/17.
 */

public class DetailShopEntity {
    String id;
    String cover;
    String name;
    int money;

    public DetailShopEntity(String id, String cover, String name, int money) {
        this.id = id;
        this.cover = cover;
        this.name = name;
        this.money = money;
    }

    public DetailShopEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
