package peko.honey.entity;

/**
 * Created by macshop1 on 12/14/17.
 */

public class SlideRestaurentEntity {
    int id;
    String image;
    String url;
    String shopname;
    String shopAddress;

    public SlideRestaurentEntity(int id, String image, String url, String shopname, String shopAddress) {
        this.id = id;
        this.image = image;
        this.url = url;
        this.shopname = shopname;
        this.shopAddress = shopAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }
}
