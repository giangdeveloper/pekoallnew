package peko.honey.entity;

/**
 * Created by macshop1 on 11/30/17.
 */

public class RedeemEntity {
    int id ;
    String address;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public RedeemEntity(int id, String address) {
        this.id = id;
        this.address = address;
    }
}
