package peko.honey.entity;

/**
 * Created by macshop1 on 11/18/17.
 */

public class BuyVoucherEntity {

    int id;
    String name;
    String nameShop;
    String des;
    String cover;
    int requirePointl;
    int totalUsingTime;

    public BuyVoucherEntity() {
    }

    public BuyVoucherEntity(int id, String name, String des, String cover, int requirePointl, int totalUsingTime) {
        this.id = id;
        this.name = name;
        this.des = des;
        this.cover = cover;
        this.requirePointl = requirePointl;
        this.totalUsingTime = totalUsingTime;
    }

    public BuyVoucherEntity(int id, String name, String nameShop, String des, String cover, int requirePointl, int totalUsingTime) {
        this.id = id;
        this.name = name;
        this.nameShop = nameShop;
        this.des = des;
        this.cover = cover;
        this.requirePointl = requirePointl;
        this.totalUsingTime = totalUsingTime;
    }

    public String getNameShop() {
        return nameShop;
    }

    public void setNameShop(String nameShop) {
        this.nameShop = nameShop;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getRequirePointl() {
        return requirePointl;
    }

    public void setRequirePointl(int requirePointl) {
        this.requirePointl = requirePointl;
    }

    public int getTotalUsingTime() {
        return totalUsingTime;
    }

    public void setTotalUsingTime(int totalUsingTime) {
        this.totalUsingTime = totalUsingTime;
    }
}
