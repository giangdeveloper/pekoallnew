package peko.honey.entity;

/**
 * Created by macshop1 on 2/6/18.
 */

public class NewsEntity {
    int id;
    String date;
    String title;
    String url;
    String image;

    public NewsEntity(int id, String date, String title, String url) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.url = url;
    }

    public NewsEntity(int id, String date, String title, String url, String image) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.url = url;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
