package peko.honey.entity;

/**
 * Created by macshop1 on 11/18/17.
 */

public class MyVoucherEntity {
    int idVoucher;
    String code;
    int discountValue;
    String nameVoucher ;
    String nameShop;
    String desVoucher;
    String coverVoucher;
    String endUsingTime;
    boolean isused;

    public MyVoucherEntity() {
    }

    public MyVoucherEntity(int idVoucher, String code,int discountValue, String nameVoucher,String nameShop, String desVoucher, String coverVoucher, String endUsingTime) {
        this.idVoucher = idVoucher;
        this.code =code;
        this.discountValue = discountValue;
        this.nameVoucher = nameVoucher;
        this.nameShop = nameShop;
        this.desVoucher = desVoucher;
        this.coverVoucher = coverVoucher;
        this.endUsingTime = endUsingTime;
    }

    public MyVoucherEntity(int idVoucher, String code, int discountValue, String nameVoucher, String nameShop, String desVoucher, String coverVoucher, String endUsingTime, boolean isused) {
        this.idVoucher = idVoucher;
        this.code = code;
        this.discountValue = discountValue;
        this.nameVoucher = nameVoucher;
        this.nameShop = nameShop;
        this.desVoucher = desVoucher;
        this.coverVoucher = coverVoucher;
        this.endUsingTime = endUsingTime;
        this.isused = isused;
    }

    public boolean isIsused() {
        return isused;
    }

    public void setIsused(boolean isused) {
        this.isused = isused;
    }

    public String getNameShop() {
        return nameShop;
    }

    public void setNameShop(String nameShop) {
        this.nameShop = nameShop;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getIdVoucher() {
        return idVoucher;
    }

    public void setIdVoucher(int idVoucher) {
        this.idVoucher = idVoucher;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    public String getNameVoucher() {
        return nameVoucher;
    }

    public void setNameVoucher(String nameVoucher) {
        this.nameVoucher = nameVoucher;
    }

    public String getDesVoucher() {
        return desVoucher;
    }

    public void setDesVoucher(String desVoucher) {
        this.desVoucher = desVoucher;
    }

    public String getCoverVoucher() {
        return coverVoucher;
    }

    public void setCoverVoucher(String coverVoucher) {
        this.coverVoucher = coverVoucher;
    }

    public String getEndUsingTime() {
        return endUsingTime;
    }

    public void setEndUsingTime(String endUsingTime) {
        this.endUsingTime = endUsingTime;
    }
}
