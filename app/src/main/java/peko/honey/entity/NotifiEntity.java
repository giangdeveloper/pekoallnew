package peko.honey.entity;

/**
 * Created by macshop1 on 11/27/17.
 */

public class NotifiEntity {
    int id;
    int type;
    int poin;
    String name;
    String nameType2;
    String nameVoucher;
    String avatar;
    String time;
    String content;
    String header;


    public NotifiEntity() {
    }

    public NotifiEntity(int id, int type, int poin,String name, String nameType2, String nameVoucher, String avatar, String time, String content, String header) {
        this.id = id;
        this.type = type;
        this.poin = poin;
        this.name = name;
        this.nameType2 = nameType2;
        this.nameVoucher = nameVoucher;
        this.avatar = avatar;
        this.time = time;
        this.content = content;
        this.header = header;
    }


    public int getPoin() {
        return poin;
    }

    public void setPoin(int poin) {
        this.poin = poin;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameType2() {
        return nameType2;
    }

    public void setNameType2(String nameType2) {
        this.nameType2 = nameType2;
    }

    public String getNameVoucher() {
        return nameVoucher;
    }

    public void setNameVoucher(String nameVoucher) {
        this.nameVoucher = nameVoucher;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
