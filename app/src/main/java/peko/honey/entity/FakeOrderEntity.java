package peko.honey.entity;

/**
 * Created by macshop1 on 11/30/17.
 */

public class FakeOrderEntity {
    int price;
    String name;

    public FakeOrderEntity(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
