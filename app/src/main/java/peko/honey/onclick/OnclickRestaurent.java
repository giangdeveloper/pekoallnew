package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.RestaurentEntity;

/**
 * Created by macshop1 on 11/17/17.
 */

public interface OnclickRestaurent {

    void onclickRestaurent(View v, RestaurentEntity entity);
    void onclickExChange(View v, RestaurentEntity entity);
    void onclickViewLocation(View v, RestaurentEntity entity);
}
