package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.BuyVoucherEntity;

/**
 * Created by macshop1 on 11/28/17.
 */

public interface OnclickBuyvoucher {
    void onclickBuyVoucher(View v, BuyVoucherEntity entity);
    void onclickViewLocation(View v, BuyVoucherEntity entity);
}
