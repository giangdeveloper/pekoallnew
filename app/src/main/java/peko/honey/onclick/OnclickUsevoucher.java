package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.BuyVoucherEntity;
import peko.honey.entity.MyVoucherEntity;

/**
 * Created by macshop1 on 11/28/17.
 */

public interface OnclickUsevoucher {
    void onclickUseVoucher(View v, MyVoucherEntity entity,int pos);
    void onclickViewLocation(View v, MyVoucherEntity entity);
}
