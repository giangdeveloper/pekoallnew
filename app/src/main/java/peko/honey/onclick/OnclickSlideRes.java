package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.NewsEntity;
import peko.honey.entity.SlideRestaurentEntity;


/**
 * Created by macshop1 on 2/7/18.
 */

public interface OnclickSlideRes {

    void onclickSlide(View v, SlideRestaurentEntity entity);
}
