package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.BuyVoucherEntity;
import peko.honey.entity.FavoriteShopEntity;

/**
 * Created by macshop1 on 11/28/17.
 */

public interface OnclickFavoriteShop {
    void onclickFavorite(View v, FavoriteShopEntity entity);
    void onclickExchangeGift(View v, FavoriteShopEntity entity);
    void onclickViewLocation(View v, FavoriteShopEntity entity);
}
