package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import peko.honey.entity.ExChangeGiftEntity;
import peko.honey.onclick.OnclickExchangeGift;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Giang on 7/4/2017.
 */

public class ExchangeGiftAdapter extends RecyclerView.Adapter<ExchangeGiftAdapter.ViewHolder> {

    ArrayList<ExChangeGiftEntity> arrayList;
    Context context;
    OnclickExchangeGift onclick;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvName,tvExchangeRate;
      //  private final LinearLayout linerMonDat;
        private final ImageView imgCoverExchange;

        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvExchangeRate = (TextView)v.findViewById(R.id.tvExchangeRate);
          //  linerMonDat = (LinearLayout) v.findViewById(R.id.linerMonDat);
            imgCoverExchange = (ImageView)v.findViewById(R.id.imgCoverExchange);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public ExchangeGiftAdapter(Context context, ArrayList<ExChangeGiftEntity> arrayList,OnclickExchangeGift onclick) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclick = onclick;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_gift, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final ExChangeGiftEntity curentPost = arrayList.get(position);


        viewHolder.tvName.setText(curentPost.getName());
        viewHolder.tvExchangeRate.setText(String.valueOf(curentPost.getPoin()));

        if (curentPost.getCover() != null & !curentPost.getCover().equals("") & !curentPost.getCover().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getCover())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgCoverExchange);
        }

        viewHolder.imgCoverExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    onclick.onclickExchangeGift(view,curentPost);
            }
        });


//        viewHolder.imgCoverRes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//            }
//        });

      //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
