package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.FavoriteShopEntity;
import peko.honey.entity.RestaurentEntity;
import peko.honey.onclick.OnclickFavoriteShop;
import peko.honey.onclick.OnclickRestaurent;


/**
 * Created by Giang on 7/4/2017.
 */

public class FavoriteShopAdapter extends RecyclerView.Adapter<FavoriteShopAdapter.ViewHolder> {

    ArrayList<FavoriteShopEntity> arrayList;
    Context context;
    OnclickFavoriteShop onclickFavoriteShop;

    public static class ViewHolder extends RecyclerView.ViewHolder {
       private final TextView tvGiftPoin,tvName,tvLocation,tvBrand,tvDoiQua;
      //  private final LinearLayout linerMonDat;
        private final ImageView imgCoverRes;

        public ViewHolder(View v) {
            super(v);
          //  tvName = (TextView) v.findViewById(R.id.tvTenMonChon);
          //  tvTien = (TextView)v.findViewById(R.id.tvTien);
          //  linerMonDat = (LinearLayout) v.findViewById(R.id.linerMonDat);
            tvGiftPoin = (TextView)v.findViewById(R.id.tvGiftPoin);
            tvLocation = (TextView)v.findViewById(R.id.tvLocation);
            tvBrand = (TextView)v.findViewById(R.id.tvBrand);
            tvName = (TextView)v.findViewById(R.id.tvName);
            tvDoiQua = (TextView)v.findViewById(R.id.tvDoiQua);
            imgCoverRes = (ImageView)v.findViewById(R.id.imgCoverRes);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public FavoriteShopAdapter(Context context, ArrayList<FavoriteShopEntity> arrayList,OnclickFavoriteShop onclickFavoriteShop) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickFavoriteShop = onclickFavoriteShop;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_restaurent, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final FavoriteShopEntity curentPost = arrayList.get(position);

        viewHolder.tvGiftPoin.setText(context.getResources().getString(R.string.bancocoin) + " " + curentPost.getDiamond());
        viewHolder.tvName.setText(curentPost.getName());
        viewHolder.tvLocation.setText(curentPost.getLocation());
        viewHolder.tvBrand.setText(curentPost.getBrand() + " " + context.getResources().getString(R.string.coso));

        if (curentPost.getCover() != null& !curentPost.getCover().equals("") & !curentPost.getCover().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getCover())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgCoverRes);
        }

        if(curentPost.getDiamond()>=1)
            viewHolder.tvGiftPoin.setVisibility(View.VISIBLE);
        else
            viewHolder.tvGiftPoin.setVisibility(View.GONE);


        viewHolder.tvBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickFavoriteShop.onclickViewLocation(view,curentPost);
            }
        });

        viewHolder.imgCoverRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   onclickRestaurent.onclickRestaurent(view,curentPost);
            }
        });
        viewHolder.tvDoiQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickFavoriteShop.onclickExchangeGift(view,curentPost);
            }
        });

        viewHolder.imgCoverRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickFavoriteShop.onclickFavorite(view,curentPost);
            }
        });

      //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
