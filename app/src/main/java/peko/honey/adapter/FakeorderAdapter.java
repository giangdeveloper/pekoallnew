package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.FakeOrderEntity;
import peko.honey.entity.RestaurentEntity;
import peko.honey.onclick.OnclickRestaurent;


/**
 * Created by Giang on 7/4/2017.
 */

public class FakeorderAdapter extends RecyclerView.Adapter<FakeorderAdapter.ViewHolder> {

    ArrayList<FakeOrderEntity> arrayList;
    Context context;



    public static class ViewHolder extends RecyclerView.ViewHolder {
       private final TextView tvMoney,tvName;
      //  private final LinearLayout linerMonDat;

        public ViewHolder(View v) {
            super(v);
          //  tvName = (TextView) v.findViewById(R.id.tvTenMonChon);
          //  tvTien = (TextView)v.findViewById(R.id.tvTien);
          //  linerMonDat = (LinearLayout) v.findViewById(R.id.linerMonDat);
            tvMoney = (TextView)v.findViewById(R.id.tvMoney);

            tvName = (TextView)v.findViewById(R.id.tvName);


        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public FakeorderAdapter(Context context, ArrayList<FakeOrderEntity> arrayList) {
        this.context = context;
        this.arrayList = arrayList;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_fake_order, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final FakeOrderEntity curentPost = arrayList.get(position);


        viewHolder.tvName.setText(curentPost.getName());
        viewHolder.tvMoney.setText(curentPost.getPrice() + " VND");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
