package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import peko.honey.entity.RestaurentEntity;
import peko.honey.onclick.OnclickRestaurent;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Giang on 7/4/2017.
 */

public class RestaurentAdapter extends RecyclerView.Adapter<RestaurentAdapter.ViewHolder> {

    ArrayList<RestaurentEntity> arrayList;
    Context context;
    OnclickRestaurent onclickRestaurent;


    public static class ViewHolder extends RecyclerView.ViewHolder {
       private final TextView tvGiftPoin,tvName,tvLocation,tvBrand,tvDoiQua;
      //  private final LinearLayout linerMonDat;
        private final ImageView imgCoverRes;

        public ViewHolder(View v) {
            super(v);
          //  tvName = (TextView) v.findViewById(R.id.tvTenMonChon);
          //  tvTien = (TextView)v.findViewById(R.id.tvTien);
          //  linerMonDat = (LinearLayout) v.findViewById(R.id.linerMonDat);
            tvGiftPoin = (TextView)v.findViewById(R.id.tvGiftPoin);
            tvLocation = (TextView)v.findViewById(R.id.tvLocation);
            tvBrand = (TextView)v.findViewById(R.id.tvBrand);
            tvName = (TextView)v.findViewById(R.id.tvName);
            tvDoiQua = (TextView)v.findViewById(R.id.tvDoiQua);
            imgCoverRes = (ImageView)v.findViewById(R.id.imgCoverRes);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public RestaurentAdapter(Context context, ArrayList<RestaurentEntity> arrayList,OnclickRestaurent onclickRestaurent) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickRestaurent = onclickRestaurent;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_restaurent, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final RestaurentEntity curentPost = arrayList.get(position);


        if(curentPost.getExchangerate() == 0)
            viewHolder.tvDoiQua.setVisibility(View.GONE);
        else
            viewHolder.tvDoiQua.setVisibility(View.VISIBLE);


        viewHolder.tvGiftPoin.setText(context.getResources().getString(R.string.banco) + " " + curentPost.getDiamond());
        viewHolder.tvName.setText(curentPost.getName());
        viewHolder.tvLocation.setText(curentPost.getLocation());
        viewHolder.tvBrand.setText(curentPost.getBrand() + " " + context.getResources().getString(R.string.coso));

//        if(curentPost.getDiamond()==0)
//            viewHolder.tvGiftPoin.setVisibility(View.GONE);

        if (curentPost.getCover() != null & !curentPost.getCover().equals("") & !curentPost.getCover().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getCover())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgCoverRes);
        }

        viewHolder.tvBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickRestaurent.onclickViewLocation(view,curentPost);
            }
        });


        viewHolder.imgCoverRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickRestaurent.onclickRestaurent(view,curentPost);
            }
        });
        viewHolder.tvDoiQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickRestaurent.onclickExChange(view,curentPost);
            }
        });

      //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
