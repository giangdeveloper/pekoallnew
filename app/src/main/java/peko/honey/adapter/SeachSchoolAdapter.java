package peko.honey.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;

import java.util.ArrayList;
import java.util.List;

import peko.honey.entity.SeachSchoolEntity;

/**
 * Created by macshop1 on 12/3/17.
 */

public class SeachSchoolAdapter extends ArrayAdapter<SeachSchoolEntity> {
    private LayoutInflater layoutInflater;
    ArrayList<SeachSchoolEntity> mCustomers;

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((SeachSchoolEntity)resultValue).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<SeachSchoolEntity> suggestions = new ArrayList<SeachSchoolEntity>();
                for (SeachSchoolEntity customer : mCustomers) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.getUtfname().toLowerCase().contains(constraint.toString().toLowerCase())
                            | customer.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);

                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<SeachSchoolEntity>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mCustomers);
            }
            notifyDataSetChanged();
        }
    };

    public SeachSchoolAdapter(Context context, int textViewResourceId, List<SeachSchoolEntity> customers) {
        super(context, textViewResourceId, customers);
        // copy all the customers into a master list
        mCustomers = new ArrayList<SeachSchoolEntity>(customers.size());
        mCustomers.addAll(customers);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_searchschool, null);
        }

        SeachSchoolEntity customer = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.tvSchoolName);
        name.setText(customer.getName());
//
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                System.out.println("click day");
//                selectedCustomer = (Customer)view.getTag();
//                zoekenFragment.startSearch(selectedCustomer);
//            }
//        });


        return view;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
}
