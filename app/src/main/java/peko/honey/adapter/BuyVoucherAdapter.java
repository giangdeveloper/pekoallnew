package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import peko.honey.entity.BuyVoucherEntity;
import peko.honey.onclick.OnclickBuyvoucher;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Giang on 7/4/2017.
 */

public class BuyVoucherAdapter extends RecyclerView.Adapter<BuyVoucherAdapter.ViewHolder> {

    ArrayList<BuyVoucherEntity> arrayList;
    Context context;
    OnclickBuyvoucher onclickBuyvoucher;


    public static class ViewHolder extends RecyclerView.ViewHolder {
       private final TextView tvPoin,tvName,tvDes,tvBuy,tvNumber;
        private final LinearLayout linerBuyVoucher;
        RelativeLayout relayLocation;
        private final ImageView imgCover;

        public ViewHolder(View v) {
            super(v);
            tvPoin = (TextView) v.findViewById(R.id.tvPoin);
            tvName = (TextView)v.findViewById(R.id.tvName);
            tvDes = (TextView)v.findViewById(R.id.tvDes);
            tvBuy = (TextView)v.findViewById(R.id.tvBuy);
            tvNumber = (TextView)v.findViewById(R.id.tvNumber);
            linerBuyVoucher = (LinearLayout) v.findViewById(R.id.linerBuyVoucher);
            relayLocation = (RelativeLayout)v.findViewById(R.id.relayLocation);
            imgCover = (ImageView)v.findViewById(R.id.imgCover);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public BuyVoucherAdapter(Context context, ArrayList<BuyVoucherEntity> arrayList , OnclickBuyvoucher onclickBuyvoucher) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickBuyvoucher = onclickBuyvoucher;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_buyvoucher, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final BuyVoucherEntity curentPost = arrayList.get(position);

        if (curentPost.getCover() != null & !curentPost.getCover().equals("") & !curentPost.getCover().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getCover())
                    .fit()
                    .error(R.drawable.bg_shopdefault)
                    .centerInside()
                    .into(viewHolder.imgCover);
        }

        viewHolder.tvBuy.setText(context.getResources().getString(R.string.buy));
        viewHolder.tvName.setText(curentPost.getNameShop() + " - " + curentPost.getName());
        viewHolder.tvDes.setText(curentPost.getDes());
        viewHolder.tvPoin.setText(String.valueOf(curentPost.getRequirePointl()));
        viewHolder.tvNumber.setText(context.getResources().getString(R.string.soluongcon) + " " + curentPost.getTotalUsingTime());

        viewHolder.linerBuyVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickBuyvoucher.onclickBuyVoucher(view,curentPost);
            }
        });
        viewHolder.relayLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickBuyvoucher.onclickViewLocation(view,curentPost);
            }
        });

      //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
