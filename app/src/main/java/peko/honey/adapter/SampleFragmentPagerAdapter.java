package peko.honey.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import peko.honey.common.MyFragment;

import java.util.ArrayList;

/**
 * Created by macshop1 on 11/18/17.
 */

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<MyFragment> fragments = new ArrayList<>();
    private String[] titles;

    public SampleFragmentPagerAdapter(FragmentManager fm, ArrayList<MyFragment> fragments, String[] titles) {
        super(fm);
        this.fragments = fragments;
        this.titles = titles;
    }

    @Override
    public MyFragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}