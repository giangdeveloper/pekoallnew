package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.BuyVoucherEntity;
import peko.honey.entity.NotifiEntity;


/**
 * Created by Giang on 7/4/2017.
 */

public class NotifiAdapter extends RecyclerView.Adapter<NotifiAdapter.ViewHolder> {

    ArrayList<NotifiEntity> arrayList;
    Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvName, tvTime;
        //  private final LinearLayout linerMonDat;
        private final ImageView imgAvatar, imgIcon;

        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            imgAvatar = (ImageView) v.findViewById(R.id.imgAvatar);
            imgIcon = (ImageView) v.findViewById(R.id.imgIcon);
            //  linerMonDat = (LinearLayout) v.findViewById(R.id.linerMonDat);
            //imgCoverRes = (ImageView)v.findViewById(R.id.imgCoverRes);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public NotifiAdapter(Context context, ArrayList<NotifiEntity> arrayList) {
        this.context = context;
        this.arrayList = arrayList;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_notifi, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final NotifiEntity curentPost = arrayList.get(position);


        if (curentPost.getType() == 1) {

            viewHolder.tvName.setText(Html.fromHtml(context.getResources().getString(R.string.bandanhanduoc) + " " + getColoredSpanned(curentPost.getPoin() + " " + context.getResources().getString(R.string.diemtichluy),"#000000") + " " + context.getResources().getString(R.string.tucuahang) + " " + getColoredSpanned(curentPost.getName(),"#000000")));

            viewHolder.imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_honey_notifi));

            viewHolder.tvTime.setText(curentPost.getTime());

            if (curentPost.getAvatar() != null & !curentPost.getAvatar().equals("") & !curentPost.getAvatar().isEmpty()) {
                Picasso.with(context)
                        .load(curentPost.getAvatar())
                        .fit()
                        .centerCrop()
                        .error(R.drawable.ic_star)
                        .into(viewHolder.imgAvatar);
            }

        } else if (curentPost.getType() == 2) {

            viewHolder.tvName.setText(Html.fromHtml(context.getResources().getString(R.string.chucmungban) + " " + getColoredSpanned(curentPost.getNameType2(), "#000000") + " " + context.getResources().getString(R.string.tucuahang) + " " + getColoredSpanned(curentPost.getName(), "#000000")));

            viewHolder.imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_gift_notifi));

            viewHolder.tvTime.setText(curentPost.getTime());

            if (curentPost.getAvatar() != null & !curentPost.getAvatar().equals("") & !curentPost.getAvatar().isEmpty()) {
                Picasso.with(context)
                        .load(curentPost.getAvatar())
                        .fit()
                        .centerCrop()
                        .error(R.drawable.ic_star)
                        .into(viewHolder.imgAvatar);
            }

        } else if (curentPost.getType() == 3) {

            viewHolder.tvName.setText(curentPost.getHeader() + " : " + curentPost.getContent());
            viewHolder.imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_alarm_notifi));
            viewHolder.tvTime.setText(curentPost.getTime());
        } else if (curentPost.getType() == 6) {
            viewHolder.tvName.setText(Html.fromHtml(context.getResources().getString(R.string.chucmungbanvoucher) + " " + getColoredSpanned(curentPost.getNameVoucher(),"#000000")+ " " + context.getResources().getString(R.string.tucuahang) + " " + getColoredSpanned(curentPost.getName(), "#000000")));

            viewHolder.imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_discount_notifi));

            viewHolder.tvTime.setText(curentPost.getTime());

            if (curentPost.getAvatar() != null & !curentPost.getAvatar().equals("") & !curentPost.getAvatar().isEmpty()) {
                Picasso.with(context)
                        .load(curentPost.getAvatar())
                        .fit()
                        .centerCrop()
                        .error(R.drawable.ic_star)
                        .into(viewHolder.imgAvatar);
            }
        }


        //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }

    public String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

}
