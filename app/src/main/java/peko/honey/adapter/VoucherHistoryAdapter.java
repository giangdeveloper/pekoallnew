package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.VoucherHistoryEntity;
import peko.honey.onclick.OnclickVoucherHistory;


/**
 * Created by Giang on 7/4/2017.
 */

public class VoucherHistoryAdapter extends RecyclerView.Adapter<VoucherHistoryAdapter.ViewHolder> {

    ArrayList<VoucherHistoryEntity> arrayList;
    Context context;
    OnclickVoucherHistory onclickVoucherHistory;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPoin, tvBuy, tvNumber, tvName, tvDes;
        private final ImageView imgCover, imgLocation;
        private final LinearLayout linerBuyVoucher;
        private final RelativeLayout relayLocation;
        //  private final ImageView imgCoverRes;

        public ViewHolder(View v) {
            super(v);
            tvPoin = (TextView) v.findViewById(R.id.tvPoin);
            tvBuy = (TextView) v.findViewById(R.id.tvBuy);
            tvNumber = (TextView) v.findViewById(R.id.tvNumber);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvDes = (TextView) v.findViewById(R.id.tvDes);
            imgCover = (ImageView) v.findViewById(R.id.imgCover);
            imgLocation = (ImageView) v.findViewById(R.id.imgLocation);
            relayLocation = (RelativeLayout) v.findViewById(R.id.relayLocation);
            linerBuyVoucher = (LinearLayout) v.findViewById(R.id.linerBuyVoucher);

        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public VoucherHistoryAdapter(Context context, ArrayList<VoucherHistoryEntity> arrayList ,  OnclickVoucherHistory onclickVoucherHistory) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickVoucherHistory = onclickVoucherHistory;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_voucher_history, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final VoucherHistoryEntity curentPost = arrayList.get(position);

        if (curentPost.getCover() != null & !curentPost.getCover().equals("") & !curentPost.getCover().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getCover())
                    .fit()
                    .centerInside()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgCover);
        }

        viewHolder.tvName.setText(curentPost.getName());
        viewHolder.tvPoin.setText(String.valueOf(curentPost.getRequirePointl()));

        viewHolder.relayLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onclickVoucherHistory.onclickViewLocation(view,curentPost);

            }
        });

        //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
