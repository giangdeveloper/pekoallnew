package peko.honey.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.IntentCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.LoginActivity;
import peko.honey.Main2Activity;
import peko.honey.MainActivity;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;

/**
 * Created by macshop1 on 11/20/17.
 */

public class InputPassFragment extends MyFragment implements ApiHandle.ApiCallback, ApiConstants {
    View root;
    public static String TAG = "InputPassFragment";
    TextView tvLogIn;
    EditText edPassWord;
    LoginActivity activity;
    String phone, token, username, email, avatar, fullName, referralCode, currentPhone;
    int id, totalPoints;
    boolean isAlive,verified;
    CatLoadingView mView;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.inputpass_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvLogIn = (TextView) findViewById(R.id.tvLogIn);
        activity = (LoginActivity) getActivity();
        edPassWord = (EditText) findViewById(R.id.edPassWord);
        activity.refreshActionBar(getView());

        isAlive = true;

        edPassWord.requestFocus();
        showKeyBoard();


        currentPhone = getArguments().getString("phone");

        tvLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edPassWord.getText().length() >= 6) {

                    if (!hasPermissions(getActivity(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                    } else {
                        hideSoftKeyboard(getActivity());
                        loginPhone(edPassWord.getText().toString());
                    }
                }
            }
        });

        edPassWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (edPassWord.getText().length() >= 6) {
                    tvLogIn.setTextColor(getResources().getColor(R.color.xamdam));
                } else {
                    if (tvLogIn.getCurrentTextColor() == getResources().getColor(R.color.xamdam)) {
                        tvLogIn.setTextColor(getResources().getColor(R.color.xamnhe));
                    }
                }

            }
        });

    }


    void loginPhone(String pass) {


        if (currentPhone.matches("") | currentPhone.trim().length() <= 0) {
            showToast("Mời nhập số điện thoại");
            return;
        }

        if (currentPhone.trim().length() <= 8) {
            showToast("Số điện thoại không đủ dài");
            return;
        }

        if (pass.matches("Mời nhập mật khẩu") | pass.trim().length() <= 0) {
            showToast("");
            return;
        }

        if (pass.trim().length() <= 7) {
            showToast("Mật khẩu không đủ dài");
            return;
        }


        if (currentPhone.substring(0, 1).equals("0")) {
            ApiHandle.loginPhone(API_LOGIN, this, getActivity(), currentPhone, pass);
        } else {
            currentPhone = 0 + currentPhone;
            ApiHandle.loginPhone(API_LOGIN, this, getActivity(), 0 + currentPhone, pass);
        }


        mView = new CatLoadingView();
        mView.setCancelable(false);
        // mView.setCanceledOnTouchOutside(false);
        mView.show(getFragmentManager(), "");
    }


    @Override
    public void onProgress(String api) {


    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {

            mView.dismiss();

            if (result != null) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (isDoesString(jsonObject, "status")) {
                        showToast(jsonObject.getString("message"));
                    } else {

                        JSONObject jsonObjectUser = new JSONObject(jsonObject.getString("user"));

                        if (isDoesString(jsonObject, "token"))
                            token = jsonObject.getString("token");

                        if (isDoesString(jsonObjectUser, "id"))
                            id = jsonObjectUser.getInt("id");

                        if (isDoesString(jsonObjectUser, "username"))
                            username = jsonObjectUser.getString("username");

                        if (isDoesString(jsonObjectUser, "email"))
                            email = jsonObjectUser.getString("email");

                        if (isDoesString(jsonObjectUser, "phone"))
                            phone = jsonObjectUser.getString("phone");

                        if (isDoesString(jsonObjectUser, "avatar"))
                            avatar = jsonObjectUser.getString("avatar");

                        if (isDoesString(jsonObjectUser, "fullName"))
                            fullName = jsonObjectUser.getString("fullName");


                        if (isDoesString(jsonObjectUser, "verified"))
                            verified = jsonObjectUser.getBoolean("verified");

                        if (isDoesString(jsonObjectUser, "referralCode"))
                            referralCode = jsonObjectUser.getString("referralCode");

                        if (isDoesString(jsonObjectUser, "totalPoints"))
                            totalPoints = jsonObjectUser.getInt("totalPoints");

                        pref = getActivity().getSharedPreferences("CheckAccess", 0);// 0 - là chế độ private
                        editor = pref.edit();
                        editor.putString("Token", token);
                        editor.putInt("id", id);
                        editor.putBoolean("isVerifi", verified);
                        editor.putString("username", username);
                        editor.putString("email", email);
                        editor.putInt("staticPoin",0);
                        editor.putString("schoolName","");
                        editor.putString("success",null);
                        editor.putString("phone", phone);
                        editor.putString("avatar", avatar);
                        editor.putString("fullName", fullName);
                        editor.putString("referralCode", referralCode);
                        editor.putInt("totalPoints", totalPoints);
                        editor.commit();


                        Intent intent = new Intent(getActivity(), Main2Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
            }
        }

    }

    //check android 6.0
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}