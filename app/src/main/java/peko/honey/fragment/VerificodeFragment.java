package peko.honey.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.peko.neko.pekoallnews.R;

import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.otpview.OtpView;

/**
 * Created by macshop1 on 11/14/17.
 */

public class VerificodeFragment extends MyFragment implements ApiConstants,ApiHandle.ApiCallback{

    public static String TAG = "VerificodeFragment";
    View root;
    OtpView mOtpView;
    String phone;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.verificode_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mOtpView = (OtpView) findViewById(R.id.otp_view);

        mOtpView.setOTP("");
        mOtpView.setFocusable(true);
        showKeyBoard();

        phone = getArguments().getString("phone");

       // mOtpView.

       // System.out.println("otp : " + mOtpView.getOTP());

        sendVerifiPhone(phone);

    }

    void sendVerifiPhone(String phone)
    {
        ApiHandle.verifiPhone(API_VERIFI_PHONE,phone,this,getActivity());
    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

    }


}
