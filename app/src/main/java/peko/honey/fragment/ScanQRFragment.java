package peko.honey.fragment;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.Main2Activity;
import peko.honey.MainActivity;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.readToken.WriteReadToken;

import static peko.honey.fragment.BuyVoucherFragment.ACCESS_PREFERENCES;

/**
 * Created by Giang on 8/18/2017.
 */

public class ScanQRFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {
    public static final String TAG = "ScanQRFragment";
    View root;
    TextView tvTittle;
    TextView tvUserName, tvPhone, tvEdit;
    Main2Activity activity;
    boolean isAlive;
    ImageView imgScanQR;
    String Phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        root = inflater.inflate(R.layout.scanqr_fragment, container, false);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        activity = (Main2Activity) getActivity();
        tvTittle = (TextView) findViewById(R.id.tvTittle);

        tvPhone = (TextView) findViewById(R.id.tvPhone);
        tvEdit = (TextView) findViewById(R.id.tvEdit);
        imgScanQR = (ImageView) findViewById(R.id.imgScanQR);
        activity.refreshActionBar(getView());
        tvTittle.setText(getResources().getString(R.string.quetmaqr));

        isAlive = true;


        if (WriteReadToken.phone != null & !WriteReadToken.phone.equals("") & !WriteReadToken.phone.isEmpty() & !WriteReadToken.phone.equals("null") & WriteReadToken.verified == true)
            tvPhone.setText("+" + WriteReadToken.phone.substring(0, 2) + " " + WriteReadToken.phone.substring(2, WriteReadToken.phone.length()));

        if (WriteReadToken.avatar != null & !WriteReadToken.avatar.equals("") & !WriteReadToken.avatar.isEmpty()) {
            Picasso.with(getActivity())
                    .load(WriteReadToken.avatar)
                    .fit()
                    .centerCrop()
                    .into(imgScanQR);
        }

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogAddNumber();
            }
        });
    }

    void sendVerifiPhone(String phone) {
        ApiHandle.verifiPhone(API_VERIFI_PHONE, phone, this, getActivity());
    }

    public void putOTP(String code) {
        ApiHandle.PutOTP(API_VERIFI_PHONE, code, this, getActivity());
    }

    public void showDialogAddNumber() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.inputphone_dialog);


        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        final EditText edSoDienThoai = (EditText) dialog.findViewById(R.id.edSoDienThoai);
        RelativeLayout relayX = (RelativeLayout) dialog.findViewById(R.id.relayX);


        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edSoDienThoai.getText().toString().matches("") | edSoDienThoai.getText().toString().trim().length() <= 9) {
                    showToast("Số điện thoại không đúng định dạng");
                    return;
                } else {
                    Phone = edSoDienThoai.getText().toString();
                    sendVerifiPhone(edSoDienThoai.getText().toString());
                    //  sendOTP(edSoDienThoai.getText().toString());
                    dialog.dismiss();
                }

            }
        });

        relayX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }


    public void showDialogHaveVerifired() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.have_verifired_dialog);

        RelativeLayout relayX = (RelativeLayout) dialog.findViewById(R.id.relayX);
        final EditText edMaXacNhan = (EditText) dialog.findViewById(R.id.edMaXacNhan);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        relayX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edMaXacNhan.getText().toString().matches("") | edMaXacNhan.getText().toString().trim().length() <= 3) {
                    showToast("Mời bạn nhập mã xác nhận");
                    return;
                } else {

                    putOTP(edMaXacNhan.getText().toString());
                    //  putCode = true;
                    // showToast("Đang xử lí ...");
                    // putOTP(edMaXacNhan.getText().toString());
                    dialog.dismiss();
                }

            }
        });

        dialog.show();


    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (api.equals(API_VERIFI_PHONE)) {

                if (result != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has("status")) {
                            int status = jsonObject.getInt("status");
                            if (status == 200) {

                                //  sendOTP(phone);
                                showDialogHaveVerifired();
                                //   showToast("Bạn đã thay đổi số điện thoại thành công");

                            } else {
                                showDialogMess(jsonObject.getString("message"), getActivity());
                            }
                        } else {
                            showDialogHaveVerifired();
                            // sendOTP(phone);
                            // showDialogDaCoMa();
                            // showToast("Bạn đã thay đổi số điện thoại thành công");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
                }
            } else if (api.equals(API_VERIFICODE)) {

                if (result != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has("status")) {
                            int status = jsonObject.getInt("status");
                            if (status == 200) {

                                showDialogMess("Bạn đã thay đổi số điện thoại thành công", getActivity());

                            } else {
                                showDialogMess(jsonObject.getString("message"), getActivity());
                            }
                        } else {

                            SharedPreferences settings = getActivity().getSharedPreferences(ACCESS_PREFERENCES, getActivity().MODE_PRIVATE);
                            SharedPreferences.Editor prefEditor = settings.edit();
                            prefEditor.putString("phone", Phone);
                            prefEditor.commit();

                            WriteReadToken writeReadToken = new WriteReadToken();
                            writeReadToken.getPref("CheckAccess", getActivity());

                            showDialogMess("Bạn đã thay đổi số điện thoại thành công", getActivity());

                            if (WriteReadToken.phone != null & !WriteReadToken.phone.equals("") & !WriteReadToken.phone.isEmpty() & !WriteReadToken.phone.equals("null") & WriteReadToken.verified == true)
                                tvPhone.setText(WriteReadToken.phone);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
                }
            }

        }
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
