package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peko.neko.pekoallnews.R;
import peko.honey.common.MyFragment;

/**
 * Created by macshop1 on 11/17/17.
 */

public class PekoPassFragment extends MyFragment {

    public static String TAG = "PekoPassFragment";
    View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.pekopass_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

}