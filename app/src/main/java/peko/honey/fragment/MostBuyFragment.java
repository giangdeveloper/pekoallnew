package peko.honey.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.adapter.ExchangeGiftAdapter;
import peko.honey.adapter.RedeemAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.ExChangeGiftEntity;
import peko.honey.entity.RedeemEntity;
import peko.honey.onclick.OnclickExchangeGift;
import peko.honey.onclick.OnclickRedeem;
import peko.honey.readToken.WriteReadToken;

import java.util.ArrayList;

/**
 * Created by macshop1 on 11/17/17.
 */

public class MostBuyFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback, OnclickRedeem, OnclickExchangeGift {

    public static String TAG = "MostBuyFragment";
    View root;
    ExchangeGiftAdapter adapter;
    ExChangeGiftEntity entity;
    RedeemEntity rEntity;
    RedeemAdapter rAdaper;
    ArrayList<RedeemEntity> resultRedeem = new ArrayList<>();
    ArrayList<ExChangeGiftEntity> results = new ArrayList<>();
    ShimmerRecyclerView rcMostBuy;
    LinearLayoutManager mLayoutManager;
    GridLayoutManager lLayout;
    int diamond, exchageRate, id, requirePoints, idShop, favorite, currentIdShop, currentIdReWard;
    int idBranches, idCard, currentIdBranches, totalPoin;
    String nameShop, cover, name, addressBranches;
    boolean isAlive;
    CatLoadingView mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.mostbuy_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcMostBuy = (ShimmerRecyclerView) findViewById(R.id.rcMostBuy);


        isAlive = true;


        //  rcMostBuy.setAdapter(adapter);thành
        getExchangeGift();

    }

    public void setPoin(int Poin) {
        totalPoin = Poin;
    }

    public void setID(int id, int isfavorite) {
        idShop = id;
        favorite = isfavorite;
    }

    void getExchangeGift() {
        ApiHandle.getExchangeGift(API_DETAILSHOP + idShop, this, getActivity());
    }

    void reDeem(String total, String pin) {
        ApiHandle.reeDeem(API_REDEEM + currentIdShop + CARD + idCard + COLLECT, this, getActivity(), total, pin);
    }

    void exchangeGift(String code) {

        ApiHandle.getReWard(API_REDEEM + currentIdShop + BRANCHES + currentIdBranches + CARD + idCard + REWARDS + currentIdReWard + RECEIVE, code, this, getActivity());
    }

    @Override
    public void onProgress(String api) {
        if (api.equals(API_DETAILSHOP + idShop))
            rcMostBuy.showShimmerAdapter();
        else if (api.equals(API_REDEEM + currentIdShop + CARD + idCard + COLLECT)) {
            // mView = new CatLoadingView();
            // mView.setCancelable(false);
            // mView.setCanceledOnTouchOutside(false);
            // mView.show(getFragmentManager(), "");
        }
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {

            if (api.equals(API_DETAILSHOP + idShop)) {
                if (result != null) {
                    rcMostBuy.hideShimmerAdapter();

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (isDoesString(jsonObject, "id"))
                            idCard = jsonObject.getInt("id");

                        JSONObject jsonObjectShop = new JSONObject(jsonObject.getString("shop"));

                        if (isDoesString(jsonObjectShop, "id"))
                            currentIdShop = jsonObjectShop.getInt("id");

                        JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));

                        JSONObject jsonObjectBranches1 = jsonArrayBranches.getJSONObject(0);

                        if (isDoesString(jsonObjectBranches1, "id"))
                            currentIdBranches = jsonObjectBranches1.getInt("id");

                        for (int k = 0; k < jsonArrayBranches.length(); k++) {
                            JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(k);

                            if (isDoesString(jsonObjectBranches, "id"))
                                idBranches = jsonObjectBranches.getInt("id");

                            if (isDoesString(jsonObjectBranches, "address"))
                                addressBranches = jsonObjectBranches.getString("address");

                            rEntity = new RedeemEntity(idBranches, addressBranches);
                            resultRedeem.add(rEntity);
                        }

                        if (isDoesString(jsonObject, "exchangeRate"))
                            exchageRate = jsonObject.getInt("exchangeRate");


                        if (isDoesString(jsonObject, "giftPoints"))
                            diamond = jsonObject.getInt("giftPoints");

                        JSONArray jsonArrayCardRewards = new JSONArray(jsonObject.getString("cardRewards"));
                        for (int i = 0; i < jsonArrayCardRewards.length(); i++) {

                            JSONObject jsonObjectCard = jsonArrayCardRewards.getJSONObject(i);

                            if (isDoesString(jsonObjectCard, "id"))
                                id = jsonObjectCard.getInt("id");

                            if (isDoesString(jsonObjectCard, "name"))
                                name = jsonObjectCard.getString("name");

                            if (isDoesString(jsonObjectCard, "image"))
                                cover = jsonObjectCard.getString("image");

                            if (isDoesString(jsonObjectCard, "requirePoints"))
                                requirePoints = jsonObjectCard.getInt("requirePoints");


                            entity = new ExChangeGiftEntity(id, cover, name, requirePoints);
                            results.add(entity);
                        }

                        lLayout = new GridLayoutManager(getActivity(), 2);
                        rcMostBuy.setHasFixedSize(true);

                        rcMostBuy.setLayoutManager(lLayout);

                        adapter = new ExchangeGiftAdapter(getContext(), results, this);
                        rcMostBuy.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    getExchangeGift();
                }
            } else if (api.equals(API_REDEEM + currentIdShop + CARD + idCard + COLLECT)) {

                // mView.dismiss();

                if (result != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (isDoesString(jsonObject, "points")) {

                        } else {
                            showDialogMess(jsonObject.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    showDialogMess("Có vấn đề về mạng , vui lòng thử lại ", getActivity());
                }
            } else if (api.equals(API_REDEEM + currentIdShop + BRANCHES + currentIdBranches + CARD + idCard + REWARDS + currentIdReWard + RECEIVE)) {
                if (result != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (isDoesString(jsonObject, "status")) {
                            showDialogMess(jsonObject.getString("message"), getActivity());
                        } else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
                }
            }

        }


    }


    public void showDialogRedeem() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.redeem_dialog);

        RecyclerView rcRedeem = (RecyclerView) dialog.findViewById(R.id.rcRedeem);

        TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
        ImageView imgProfileBuyVoucher = (ImageView) dialog.findViewById(R.id.imgProfileBuyVoucher);
        TextView tvPhone = (TextView) dialog.findViewById(R.id.tvPhone);
        TextView tvNameBuyVoucher = (TextView) dialog.findViewById(R.id.tvNameBuyVoucher);
        final EditText edInputPin = (EditText) dialog.findViewById(R.id.edInputPin);

        tvNameBuyVoucher.setText(WriteReadToken.fullName);

        if (WriteReadToken.phone != null & !WriteReadToken.phone.equals("") & !WriteReadToken.phone.isEmpty() & !WriteReadToken.phone.equals("null") & WriteReadToken.verified == true)
            tvPhone.setText("+" + WriteReadToken.phone.substring(0, 2) + " " + WriteReadToken.phone.substring(2, WriteReadToken.phone.length()));


        if (WriteReadToken.avatar != null & !WriteReadToken.avatar.equals("") & !WriteReadToken.avatar.isEmpty()) {
            Picasso.with(getActivity())
                    .load(WriteReadToken.avatar)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(imgProfileBuyVoucher);
        }


        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcRedeem.setLayoutManager(mLayoutManager);

        rAdaper = new RedeemAdapter(getActivity(), resultRedeem, this);
        rcRedeem.setAdapter(rAdaper);

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }


    public void showInPutMoneynPass() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.input_pinnpass_dialog);

        final EditText edInputPin = (EditText) dialog.findViewById(R.id.edInputPin);
        final EditText edInputMoney = (EditText) dialog.findViewById(R.id.edInputMoney);


        edInputPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if ((actionId == EditorInfo.IME_ACTION_DONE)) {

                    if (edInputMoney.getText().length() < 4) {
                        showToast("Tiền chưa đủ");
                    } else if (edInputPin.getText().length() < 6) {
                        showToast("Mã pin chưa đủ");

                    } else {
                        reDeem(edInputMoney.getText().toString(), edInputPin.getText().toString());

                        dialog.dismiss();
                    }

                }
                return false;
            }
        });

        dialog.show();

    }

    public void showInPutPin() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.input_pin_dialog);

        final EditText edPin = (EditText) dialog.findViewById(R.id.edPin);

        edPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if ((actionId == EditorInfo.IME_ACTION_DONE)) {

                    if (edPin.length() < 6) {
                        showToast("Mã Pin không hợp lý");
                    } else {
                        exchangeGift(edPin.getText().toString());
                        dialog.dismiss();
                    }
                }
                return false;
            }
        });

        dialog.show();

    }

    @Override
    public void onclickRedeem(View v, RedeemEntity entity) {
        System.out.println("card : " + entity.getId());
        showInPutMoneynPass();
    }

    @Override
    public void onclickExchangeGift(View v, ExChangeGiftEntity entity) {
       // if (totalPoin >= entity.getPoin()) {
            currentIdReWard = entity.getId();
            showInPutPin();
      //  } else {
      //      showToast("Số điểm của bạn chưa đủ để đổi quà");
     //   }
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}