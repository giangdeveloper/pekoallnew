package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import peko.honey.Main2Activity;

import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.textCenter.DrawableCenterTextView;

/**
 * Created by macshop1 on 11/17/17.
 */

public class DetailShopFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {

    public static String TAG = "DetailShopFragment";
    View root;
    MostBuyFragment mostBuyFragment;
    SaleOffFragment saleOffFragment;
    PekoPassFragment pekoPassFragment;
    TabHost tabHost;
    int mCurrentTab = 0;
    Main2Activity activity;
    TextView tvDoiQua,tvChiTieu;
    String name, address, cover;
    int diamond, brands, idShop, exchangeRate, favorite;
    ImageView imgCoverDetail;
    TextView tvDiamond, tvNameDetail, tvLocationDetail, tvBrandDetail, tvTichDiem;
    boolean isAlive, isSaveShop;
    CatLoadingView mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.detailshop_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = (Main2Activity) getActivity();
        tvDoiQua = (TextView) findViewById(R.id.tvDoiQua);
        tvDiamond = (TextView) findViewById(R.id.tvDiamond);
        tvNameDetail = (TextView) findViewById(R.id.tvNameDetail);
        tvLocationDetail = (TextView) findViewById(R.id.tvLocationDetail);
        tvTichDiem = (TextView) findViewById(R.id.tvTichDiem);
        tvChiTieu = (TextView)findViewById(R.id.tvChiTieu);
        tvBrandDetail = (TextView) findViewById(R.id.tvBrandDetail);
        imgCoverDetail = (ImageView) findViewById(R.id.imgCoverDetail);
        activity.refreshActionBar(getView());

        name = getArguments().getString("name");
        favorite = getArguments().getInt("favorite", 0);
        address = getArguments().getString("address");
        cover = getArguments().getString("cover");
        diamond = getArguments().getInt("diamond");
        brands = getArguments().getInt("brand");
        idShop = getArguments().getInt("id");
        exchangeRate = getArguments().getInt("rate");

        System.out.println(" shop id :  " + idShop);

        isAlive = true;

        tvNameDetail.setText(name);
        tvLocationDetail.setText(address);
        tvBrandDetail.setText(brands + " " + getResources().getString(R.string.coso));
        tvChiTieu.setText(IntToStringNoDecimal(exchangeRate) + " VND Chi Tiêu = 1 ");


        if (favorite == 1) {
            tvDiamond.setText(getResources().getString(R.string.bancocoin) + " " + diamond);
            tvTichDiem.setVisibility(View.VISIBLE);
        } else {
            if (diamond > 0)
                tvDiamond.setText(getResources().getString(R.string.banco) + " " + diamond);
            else
                tvDiamond.setVisibility(View.GONE);

            tvDoiQua.setVisibility(View.VISIBLE);
        }

        if (cover != null) {
            Picasso.with(getActivity())
                    .load(cover)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(imgCoverDetail);
        }

        initTab();


        tvDoiQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveStore(idShop);

            }
        });
        tvTichDiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostBuyFragment.showDialogRedeem();
            }
        });

    }

    void saveStore(int id) {

        mView = new CatLoadingView();
        mView.setCancelable(false);
        // mView.setCanceledOnTouchOutside(false);
        mView.show(getFragmentManager(), "");

        ApiHandle.saveStore(API_SAVESTORE + id, this, getActivity());
    }


    private TabHost.TabSpec newTab(String tag, int titleId, int iconId, int tabContentId) {
        View indicator = LayoutInflater.from(getActivity()).inflate(
                R.layout.tab,
                (ViewGroup) findViewById(android.R.id.tabs), false);
        DrawableCenterTextView tab = (DrawableCenterTextView) indicator.findViewById(R.id.tab);
//        TextView textCount = (TextView) indicator.findViewById(R.id.textCount);
        tab.setCompoundDrawablesWithIntrinsicBounds(0, iconId, 0, 0);

        TabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(indicator);

        if (tag.equals(MostBuyFragment.TAG)) {
            tab.setText(titleId);
            tab.setTextSize(getResources().getInteger(R.integer.textTab));
        } else if (tag.equals(SaleOffFragment.TAG)) {
            tab.setText(titleId);
            tab.setTextSize(getResources().getInteger(R.integer.textTab));
        } else if (tag.equals(PekoPassFragment.TAG)) {
            tab.setText(titleId);
            tab.setTextSize(getResources().getInteger(R.integer.textTab));
            //  textCount.setVisibility(View.VISIBLE);
            //   textCount.setText("1");
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0f);
            indicator.setLayoutParams(params);
        }

        tabSpec.setContent(tabContentId);
        return tabSpec;


    }


    public void setCurrentTab(int index) {

        mCurrentTab = index;
        tabHost.setCurrentTab(mCurrentTab);

    }


    public void initTab() {
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        tabHost.addTab(newTab(MostBuyFragment.TAG, R.string.cacphanqua, 0, R.id.frameMostBuy));
        // tabHost.addTab(newTab(SaleOffFragment.TAG, R.string.saleoff, 0, R.id.frameSaleOff));
        // tabHost.addTab(newTab(PekoPassFragment.TAG, R.string.pekopass, 0, R.id.framePekoPass));

        mostBuyFragment = new MostBuyFragment();
        mostBuyFragment.setID(idShop, favorite);
        mostBuyFragment.setPoin(diamond);
        // saleOffFragment = new SaleOffFragment();
        // pekoPassFragment = new PekoPassFragment();


//        addChildFragment(imagePhimFragment, ImagePhimFragment.TAG, R.id.AnhPhim, false);
        addChildFragment(mostBuyFragment, MostBuyFragment.TAG, R.id.frameMostBuy, false);
        // addChildFragment(saleOffFragment, SaleOffFragment.TAG, R.id.frameSaleOff, false);
        // addChildFragment(pekoPassFragment, PekoPassFragment.TAG, R.id.framePekoPass, false);

        tabHost.setCurrentTab(mCurrentTab);
        for (int i = 0; i < tabHost.getTabWidget().getTabCount(); i++) {
            final int index = i;
            tabHost.getTabWidget().getChildTabViewAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCurrentTab(index);

                }
            });
        }

    }

    @Override
    public void onProgress(String api) {


    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive == true) {
            if (result != null) {
                mView.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (isDoesString(jsonObject, "status")) {
                        if (jsonObject.getInt("status") == 200) {
                            tvDoiQua.setVisibility(View.GONE);
                            isSaveShop = true;

                            if(tvDiamond.getVisibility() == View.GONE)
                                tvDiamond.setVisibility(View.VISIBLE);

                            tvDiamond.setText(getResources().getString(R.string.bancocoin) + " " + diamond);
                            tvTichDiem.setVisibility(View.VISIBLE);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                saveStore(idShop);
            }
        }

    }

    @Override
    public void onDestroy() {

        isAlive = false;

        if (isSaveShop == true)
            sendBoardCast(BROADMAIN, RELOADMAIN);

        super.onDestroy();
    }

}
