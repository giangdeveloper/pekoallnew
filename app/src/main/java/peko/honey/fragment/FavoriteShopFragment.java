package peko.honey.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import peko.honey.Main2Activity;
import peko.honey.adapter.FavoriteShopAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.FavoriteShopEntity;
import peko.honey.onclick.OnclickFavoriteShop;
import peko.honey.readToken.WriteReadToken;

/**
 * Created by macshop1 on 11/28/17.
 */

public class FavoriteShopFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback, OnclickFavoriteShop ,SwipeRefreshLayout.OnRefreshListener{
    public static String TAG = "FavoriteShopFragment";
    View root;
    RecyclerView rcFavoriteShop;
    boolean isAlive, isLoading, firstLoading, isRefresh;
    int idShop, exchangeRate, giftPoints, pagePekoShop;
    String nameShop, imgShop, addressShop,location;
    FavoriteShopEntity entity;
    FavoriteShopAdapter adapter;
    LinearLayoutManager mLayoutManager;
    ArrayList<FavoriteShopEntity> results = new ArrayList<>();
    TextView tvTittle;
    Main2Activity activity;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.favoriteshop_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcFavoriteShop = (RecyclerView) findViewById(R.id.rcFavoriteShop);
        tvTittle = (TextView) findViewById(R.id.tvTittle);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        activity = (Main2Activity) getActivity();
        activity.refreshActionBar(getView());
        tvTittle.setText(getResources().getString(R.string.cuahanguathich));

        isAlive = true;

        getFavoriteShop(pagePekoShop);
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    void getFavoriteShop(int page) {
        ApiHandle.getFavoriteShop(API_FAVORITESTORE + WriteReadToken.id + SAVE_CARD , page, this, getActivity());
    }
    void resetMain() {
        results = new ArrayList<>();
        // adapter = new RestaurentAdapter(getApplicationContext(), results, this);
        // rcResList.setAdapter(adapter);

        firstLoading = false;
        pagePekoShop = 0;
        getFavoriteShop(pagePekoShop);
    }

    @Override
    public void onProgress(String api) {
        isLoading = true;
        //rcFavoriteShop.showShimmerAdapter();
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (result != null) {

                pagePekoShop += 1;
                isLoading = false;
                if (isRefresh == true) {
                    isRefresh = false;
                    swipeRefreshLayout.setRefreshing(false);
                }

                if (firstLoading == false) {
                    firstLoading = true;

                  //  rcFavoriteShop.hideShimmerAdapter();

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        JSONArray jsonArrayCards = new JSONArray(jsonObject.getString("cards"));


                        for (int i = 0; i < jsonArrayCards.length(); i++) {
                            JSONObject jsonObjectCards = jsonArrayCards.getJSONObject(i);
                            JSONObject jsonObjectShop = new JSONObject(jsonObjectCards.getString("shop"));
                            JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
                            JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(0);


                            location = "";

                            for (int j = 0; j < jsonArrayBranches.length(); j++) {
                                JSONObject jsonObjectBranches1 = jsonArrayBranches.getJSONObject(j);

                                if (isDoesString(jsonObjectBranches, "address"))
                                    location += (jsonObjectBranches1.getString("address") + "\n \n");


                            }

                            JSONObject jsonObjectCardMember = new JSONObject(jsonObjectCards.getString("cardMember"));

                            if (isDoesString(jsonObjectCardMember, "point"))
                                giftPoints = jsonObjectCardMember.getInt("point");


                            System.out.println("lenght : " + jsonArrayBranches.length());

                            if (isDoesString(jsonObjectCards, "id"))
                                idShop = jsonObjectCards.getInt("id");

                            if (isDoesString(jsonObjectCards, "exchangeRate"))
                                exchangeRate = jsonObjectCards.getInt("exchangeRate");


                            if (isDoesString(jsonObjectShop, "cover"))
                                imgShop = jsonObjectShop.getString("cover");

                            if (isDoesString(jsonObjectShop, "name"))
                                nameShop = jsonObjectShop.getString("name");

                            if (isDoesString(jsonObjectBranches, "address"))
                                addressShop = jsonObjectBranches.getString("address");

                            entity = new FavoriteShopEntity(idShop, exchangeRate, giftPoints, imgShop, nameShop, addressShop, jsonArrayBranches.length(), "",location);
                            results.add(entity);
                        }

                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rcFavoriteShop.setLayoutManager(mLayoutManager);

                        adapter = new FavoriteShopAdapter(getActivity(), results, this);
                        rcFavoriteShop.setAdapter(adapter);

                        rcFavoriteShop.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                            }

                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                int totalItemCount = mLayoutManager.getItemCount();
                                int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                                if (totalItemCount > 1) {
                                    if (lastVisibleItem >= totalItemCount - 5) {

                                        if (!isLoading)
                                            getFavoriteShop(pagePekoShop);
                                    }
                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    try {

                        System.out.println("chay den page 1");

                        JSONObject jsonObject = new JSONObject(result);

                        JSONArray jsonArrayCards = new JSONArray(jsonObject.getString("cards"));

                        for (int i = 0; i < jsonArrayCards.length(); i++) {
                            JSONObject jsonObjectCards = jsonArrayCards.getJSONObject(i);
                            JSONObject jsonObjectShop = new JSONObject(jsonObjectCards.getString("shop"));
                            JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
                            JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(0);
                            location = "";

                            for (int j = 0; j < jsonArrayBranches.length(); j++) {
                                JSONObject jsonObjectBranches1 = jsonArrayBranches.getJSONObject(j);

                                if (isDoesString(jsonObjectBranches1, "address"))

                                    location += (jsonObjectBranches1.getString("address") + "\n \n");

                            }


                            if (isDoesString(jsonObjectCards, "id"))
                                idShop = jsonObjectCards.getInt("id");

                            if (isDoesString(jsonObjectCards, "exchangeRate"))
                                exchangeRate = jsonObjectCards.getInt("exchangeRate");

                            if (isDoesString(jsonObjectCards, "giftPoints"))
                                giftPoints = jsonObjectCards.getInt("giftPoints");

                            if (isDoesString(jsonObjectShop, "cover"))
                                imgShop = jsonObjectShop.getString("cover");

                            if (isDoesString(jsonObjectShop, "name"))
                                nameShop = jsonObjectShop.getString("name");

                            if (isDoesString(jsonObjectBranches, "address"))
                                addressShop = jsonObjectBranches.getString("address");

                            entity = new FavoriteShopEntity();
                            entity.setId(idShop);
                            entity.setExchangerate(exchangeRate);
                            entity.setDiamond(giftPoints);
                            entity.setCover(imgShop);
                            entity.setAlllcation(location);
                            entity.setName(nameShop);
                            entity.setLocation(addressShop);
                            entity.setBrand(jsonArrayBranches.length());
                            results.add(entity);
                        }

                        adapter.notifyDataSetChanged();

                        rcFavoriteShop.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                            }

                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                int totalItemCount = mLayoutManager.getItemCount();
                                int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                                if (totalItemCount > 1) {
                                    if (lastVisibleItem >= totalItemCount - 5) {

                                        if (!isLoading)
                                            getFavoriteShop(pagePekoShop);
                                    }
                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {

                getFavoriteShop(pagePekoShop);

            }
        }

    }


    @Override
    public void onclickFavorite(View v, FavoriteShopEntity entity) {

        sendBoardCastDetailShop(BROADMAIN, DETAILSHOP, entity.getDiamond(), entity.getName(), entity.getExchangerate(), entity.getId(), entity.getCover(), entity.getLocation(), entity.getBrand());

    }

    @Override
    public void onclickExchangeGift(View v, FavoriteShopEntity entity) {
        sendBoardCastExchageGift(BROADMAIN, DOIQUA, entity.getDiamond(), entity.getName(), entity.getExchangerate(), entity.getId());

    }

    @Override
    public void onclickViewLocation(View v, FavoriteShopEntity entity) {
            showDialogShowLocation(entity.getAlllcation());
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        isRefresh = true;
        resetMain();
    }


    public void showDialogShowLocation(String text) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.viewlocation_dialog_main);

        TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvLocation.setText(text);

        dialog.show();
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
