package peko.honey.other;

import android.app.Application;
import android.content.Context;

import com.onesignal.OneSignal;

import peko.honey.notifi.MyNotificationOpenedHandler;
import peko.honey.notifi.MyNotificationReceivedHandler;


/**
 * Created by Giang on 8/31/2017.
 */

public class OnSigNal extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        //MyNotificationReceivedHandler : This will be called when a notification is received while your app is running.
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler() )
                .init();
    }

}
